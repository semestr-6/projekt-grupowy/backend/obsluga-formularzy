package queries

import (
	"log"

	"github.com/jmoiron/sqlx"
	"gitlab.com/semestr-6/projekt-grupowy/backend/obsluga-formularzy/configuration"
	"gitlab.com/semestr-6/projekt-grupowy/backend/obsluga-formularzy/units/models"
)

const GET_ALL_UNITS = `
SELECT 
	u."UnitId"
	,u."UnitNamePl" 
	,u."UnitNameEn" 
	,u."UnitShortName" 
	,u."QuantityId" 
	,u."Ratio" 
	,q."QuantityNamePl"
FROM 
	units."Units" u
JOIN units."Quantities" q ON u."QuantityId" = q."QuantityId"
`

const GET_BASIC = `
SELECT
	u."UnitNamePl"
FROM
	units."Units" u
JOIN units."Quantities" q ON q."BaseUnitID" = u."UnitId"
WHERE 
	q."QuantityId" = $1`

type buff struct {
	BasicUnit string `json:"-" db:"UnitNamePl"`
}

func GetAllUnits() (units []models.GetUnit, err error) {
	db, err := sqlx.Open("postgres", configuration.ConnectionString)
	defer db.Close()

	if err != nil {
		log.Fatal(err)
		return
	}
	err = db.Select(&units, GET_ALL_UNITS)

	if err != nil {
		log.Fatal(err)
		return
	}

	for i := range units {
		buf := []buff{}
		err = db.Select(&buf, GET_BASIC, units[i].QuantityId)
		if err != nil {
			log.Fatal(err)
			return
		}

		if len(buf) > 0 {
			units[i].BasicUnit = buf[0].BasicUnit
		}
	}

	return
}
