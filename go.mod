module gitlab.com/semestr-6/projekt-grupowy/backend/obsluga-formularzy

go 1.13

require (
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.2.0
	github.com/prometheus/client_golang v1.1.0 // indirect
	github.com/rs/cors v1.7.0 // indirect
	gitlab.com/semestr-6/projekt-grupowy/backend/go-libs/http-lib v0.0.0-20190603020554-b3d6799ff62b
)
