package queries

import (
	"database/sql"
	"log"

	"gitlab.com/semestr-6/projekt-grupowy/backend/obsluga-formularzy/configuration"
	"gitlab.com/semestr-6/projekt-grupowy/backend/obsluga-formularzy/energy_resources/models"
)

const EDIT_ENERGY_RESOURCE_ATTRIBUTE_SQL = `
UPDATE energy_resources."EnergyResourcesAttributes"
SET
	"SourceId" = $1
	,"CO2Value" = $2
	,"NCVValue" = $3
	,"CO2UnitId" = $4
WHERE 
"EnergyResourceAttributeId" = $5`

const EDIT_ENERGY_RESOURCE_SQL = `
UPDATE energy_resources."EnergyResources"
SET
	"EnergyResourceName" = $1
	,"GUSResourceId" = $2
WHERE 
"EnergyResourceId" = $3;`

func EditEnergyResourceAttribute(energyResourceEdit models.EnergyResourceAttributeEdit) (err error) {
	db, err := sql.Open("postgres", configuration.ConnectionString)

	if err != nil {
		log.Fatal(err)
		return
	}

	transaction, err := db.Begin()

	if err != nil {
		log.Fatal(err)
		return
	}

	stmt, err := transaction.Prepare(EDIT_ENERGY_RESOURCE_ATTRIBUTE_SQL)

	if err != nil {
		log.Fatal(err)
		return
	}

	_, err = stmt.Exec(
		energyResourceEdit.SourceId,
		energyResourceEdit.CO2Value,
		energyResourceEdit.NCVValue,
		energyResourceEdit.CO2UnitId,
		energyResourceEdit.EnergyResourceAttributeId,
	)

	if err != nil {
		log.Fatal(err)
		return
	}

	err = stmt.Close()
	if err != nil {
		log.Fatal(err)
		return
	}

	err = transaction.Commit()
	if err != nil {
		log.Fatal(err)
	}

	transaction, err = db.Begin()

	stmt, err = transaction.Prepare(EDIT_ENERGY_RESOURCE_SQL)

	if err != nil {
		log.Fatal(err)
		return
	}

	_, err = stmt.Exec(
		energyResourceEdit.EnergyResourceName,
		energyResourceEdit.GUSResourceId,
		energyResourceEdit.EnergyResourceId,
	)

	if err != nil {
		log.Fatal(err)
		return
	}

	err = stmt.Close()
	if err != nil {
		log.Fatal(err)
		return
	}

	err = transaction.Commit()
	if err != nil {
		log.Fatal(err)
	}

	db.Close()
	return
}
